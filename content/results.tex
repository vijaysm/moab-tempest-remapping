\section{Results}
\label{sec:results}

Evaluating the performance of the in-memory, MOAB-TempestRemap (MBTR) remapping infrastructure requires recursive profiling and optimization to ensure scalability for large-scale simulations. In order to showcase the advantage of using the mesh-aware MOAB datastructure as the MCT coupler replacement, we need to understand the per task performance of the regridder in addition to the parallel point locator scalability, and overall time for remapping weight computation. Note that except for the weight application for each solution field from a source grid to a target grid, the in-memory copy of the component meshes, migration to coupler PEs, computation of intersection elements and remapping weights are done only once during the setup phase in E3SM, per coupled component model pair.


%\begin{itemize}
%	\item Conservation and high-order accuracy preservation
%	\item Scalability of the online remapping algorithm
%	\item Discussion about criticality of domain decomposition and load balancing driven from a coupler/regridding standpoint (master-slave and other partitioners)
%\end{itemize}


\subsection{Serial Performance}

We compare the total cost for computing the supermesh and the remapping weights for several source and target grid combinations through three different methods to determine the serial computational complexity. 

\begin{enumerate}
    \item \underline{ESMF:} Kd-tree based regridder and weight generation for first/second order FV$\rightarrow$FV conservative remapping
	\item \underline{TempestRemap:} Kd-tree based supermesh generation and conservative, monotonic, high-order remap operator for FV$\rightarrow$FV, SE$\rightarrow$FV, SE$\rightarrow$SE projection
	\item \underline{MBTempest:} Advancing front intersection with MOAB and conservative weight generation with TempestRemap interfaces
\end{enumerate}


%
\begin{figure*}[h]
	\centering
	\includegraphics[width=\textwidth]{figures/serial_performance_tempest.eps}
	\caption{Comparison of serial regridding computation (supermesh and projection weight generation) between ESMF, TempestRemap, and MBTempest}
	\label{fig:tempest-moab-esmf-serial-comparison}
\end{figure*}
%

Fig.~\ref{fig:tempest-moab-esmf-serial-comparison} shows the serial performance of the remappers for computing the conservative interpolator from Cubed-Sphere (CS) grids to polygonal MPAS grids of different resolutions for a FV$\rightarrow$FV field transfer. This total time includes the computation of intersection mesh or supermesh, in addition to the remapping weights with field conservation specifications. These serial runs were executed on a machine with 8x Intel Xeon(R) CPU E7-4820 @ 2.00GHz (total of 64 cores) and 1.47 TB of RAM.
As the source grid resolution increases, the advancing front intersection with linear complexity outperforms the Kd-tree intersection algorithms used by TempestRemap and ESMF. The time spent in the remapping task, including the overlap mesh generation, provides an overall metric on the single task performance when memory bandwidth or communication concerns do not dominate in a parallel run. In this comparison with three remapping software libraries, the total computational time in the fine resolution limit as $\frac{nele(source)}{nele(target)} \approx 1$ consistently increases (going diagonally from left to right in Fig.~\ref{fig:tempest-moab-esmf-serial-comparison}). We note that the serial version of TempestRemap is comparable to ESMF and can even provide better timings on the highly refined cases, while the MBTempest remapper consistently outperforms both the tools, with a 2x speedup on average. The relatively better performance in MBTempest is accomplished through the linear complexity advancing front algorithm, which further offers avenues to incorporate finer grain task or thread level parallelism to accelerate the on-node performance on multicore and GPGPU architectures. 

%
%\subsection{L2 error convergence}
%
%Compare error convergence for projection of various analytical solutions, and measure L1, L2, Linf errors. Similar to the TempestRemap poster ('https://climatemodeling.science.energy.gov/sites/default/files/presentations/TempestPoster2015\_0.pdf')


\subsection{Scalability of the MOAB Kd-tree Point Locator}

In addition to being able to compute the supermesh between $\Omega_S$ and $\Omega_T$, MOAB also offers datastructures to query source elements containing points that correspond to the target DoFs locations. This operation is critical in evaluating bilinear and biquadratic interpolator approximations for scalar variables when conservative projection is not required by the underlying coupled model. The solution interpolation for the multi-mesh case involves two distinct phases.

\begin{enumerate}
    \item \underline{Setup phase:} Use Kd-tree to build the search datastructure to locate points corresponding to vertices in the target mesh on the source mesh
    \item \underline{Run phase:} Use the elements containing the located points to compute consistent interpolation onto target mesh vertices
\end{enumerate}


Studies were performed to evaluate the strong and weak scalability of the parallel Kd-tree point search implementation in MOAB. The scalability results were generated with the \links{https://github.com/tpeterka/cian2}{CIAN2} coupling mini-app \citep{morozov_ldav16}, 
which links to MOAB to handle traversal of the unstructured grids and transfer of solution fields between the grids. For this case, a series of hexahedral and tetrahedral meshes were used to interpolate an analytical solution. By changing the basis interpolation order, and mesh resolutions, the convergence of the interpolator was verified to provide theoretical accuracy orders of convergence in the asymptotic fine limit.

The performance tests were executed on the IBM BlueGene/Q Mira at 16 MPI ranks
per node, with 2GB RAM per MPI rank, at up to 500K MPI processes.  The
strong scaling results and error convergence were computed with a grid size of $1024^3$. The solution 
interpolation on varying mesh resolutions were performed by projecting 
an analytical solution from a Tetrahedral$\rightarrow$Hexahedral$\rightarrow$Tetrahedral 
grid, with total number of points/rank varied between [2K, 32K] in the study. Note that the total DoFs in this study is much larger than typical climate production runs, and hence we use these experiments to showcase the strong scaling of the bilinear interpolation operation at the high-res limit.

%
\begin{figure*}[h]
	\centering
	\includegraphics[width=\textwidth]{figures/tet-hex-scaling-mira.eps}
	\caption{MOAB 3-d Kd-tree Point Location: Strong scaling on Mira (BG/Q)}
	\label{fig:tet-hex-scaling-mira}
\end{figure*}
%

First, the root-mean-square (RMS) error was measured in the bilinearly interpolated solution against the analytical solution, and plotted for different source and target mesh resolutions. Fig.~\ref{fig:tet-hex-scaling-mira}-(a) demonstrates that the error convergence of the interpolants match the expected theoretical second order rates, and that the error constant is proportional to ratio of source to target mesh resolution. Next, the Fig.~\ref{fig:tet-hex-scaling-mira}-(b) shows the strong scaling efficiency of around 50\% is achieved on a maximum of 512K cores (66\% of Mira). We note that the computational complexity of the Kd-tree data structure scales as $O(nlog(n))$ asymptotically, and the point location phase during initial search setup dominates the total cost on higher core counts. This is evident in the timing breakdown for each phase shown in Fig.~\ref{fig:tet-hex-scaling-mira}-(c). Since the point location is performed only once during simulation startup, while the interpolation is performed multiple times per timestep during the run, we expect the total cost of the projection for scalar variables to be amortized over transient climate simulations with fixed grids. Further investigations with optimal BVH-tree \citep{larsen1999} or R-tree implementations for these interpolation cases could help reduce the overall cost. 

The full 3-D point location and interpolation operations provided by MOAB are comparable to the implementation in Common Remapping component used in the C-Coupler \citep{liu2013} and provide relatively much stronger scalability on larger core counts \citep{ccoupler1_2014} for the remapping operation. Such higher-order interpolators for multicomponent physics variables can provide better performance in atmospheric chemistry calculations. Additionally, as component mesh resolutions are increased to sub-Km regimes, the expectations from remapping libraries such as MOAB to provide scalable search and location of points becomes important.
Currently, only the NC bilinear or biquadratic interpolation of scalar fields with subset normalization \citep{tautges_scalable_2009} is supported directly in MOAB (via Kd-tree point location and interpolation), and advancing front intersection algorithm does not make use of these data-structures. In contrast, TempestRemap and ESMF use a Kd-tree search to not only compute the location of points, but also to evaluate the supermesh $\Omega_S \bigcup \Omega_T$, and hence the computational complexity for the intersection mesh determination scales as $O(nlog (n))$, in contrast to the linear complexity ($O(n)$) of the advancing front intersection algorithm implemented in MOAB. 

%\subsection{Spatial Coupling within E3SM}
\subsection{The Parallel MBTR Remapping Algorithm}
\label{e3sm-results-section}

The MBTR online weight generation workflow within E3SM was employed to verify and test the projection of real simulation data generated during the coupled atmosphere-ocean model runs. %The atmosphere bottom surface temperature data defined on the CS spectral element mesh couples the ocean model through 
A choice was made to use the model-computed temperature on the lowest level of the atmosphere, since the heat fluxes that nonlinearly couples the atmosphere and ocean models are directly proportional to this interface temperature field.  By convention, the fluxes are computed on the ocean mesh, and hence the atmosphere temperature must be interpolated onto MPAS polygonal mesh. We use this scenario as a test case for demonstrating the strong scalability results in this section.

The atmosphere run with approximately 4 degree grid size and 11 elements per edge on a cubed-sphere (NE11) in E3SM, and the projection of its lowest level temperature onto two different MPAS meshes (with approximate grid size of 240km) are shown in Fig.~\ref{fig:projection-se-mpas}.  The conservative field projection from SE$\rightarrow$FV on a mesh with holes corresponding to land regions is given in Fig.~\ref{fig:projection-se-mpas}-(b), where the continents are shown in transparent shading. To contrast, we also present the remapped field on an MPAS mesh without holes (Fig.~\ref{fig:projection-se-mpas}-(c)) to show the differences in the remapped solutions as a function of mesh topology.

%
\begin{figure*}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/SE-Remapped-Holes-NoHoles.eps}
    \caption{Projection of the NE11 SE bottom atmospheric temperature field onto the MPAS ocean grid}
    \label{fig:projection-se-mpas}
\end{figure*}
%

\subsubsection{Scaling Comparison of Conservative Remappers (FV$\rightarrow$FV)}

The strong scaling studies for computation of remapping weights to project a FV solution field between CS grids of varying resolutions was performed on the Blues large-scale cluster (with 16 Sandy Bridge Xeon E5-2670 2.6GHz cores and 32 GB RAM per node) at ANL, and the Cori supercomputer at NERSC (with 64 Haswell Xeon E5-2698v3 2.3GHz cores and 128 GB RAM per node). Fig.~\ref{fig:blues-cori-esmf-moab} shows that the MBTR workflow consistently outperforms ESMF on both the machines as the number of processes used by the coupler is increased. The timings shown here represent the total remapping time i.e., cumulative computational time for generating the super mesh and the (conservative) remapping weights. 

%
\begin{figure*}[h]
    \centering
    \includegraphics[width=0.45\linewidth]{figures/esmf-moab-blues-cori-cscs-timings.eps} \hspace*{5mm}
    \caption{CS (E=614400 quads) $\rightarrow$ CS (E=153600 quads) remapping (\underline{-m conserve}) on LCRC/ALCF and NERSC machines}
    \label{fig:blues-cori-esmf-moab}
\end{figure*}
%

%Additionally, to compare the runtime cost between different phases, we analyzed the cost of communication to the intersection mesh computation on both the machines. 
The relatively better scaling for MOAB on the Blues cluster is due to faster hardware and memory bandwidth compared to the Cori machine. The strong scaling efficiency approaches a plateau on Cori Haswell nodes as communication costs for the coverage mesh computation start dominating the overall remapping processes, especially in the limit of $\frac{nele}{process}\rightarrow1$ at large node counts. 

%%
%\begin{figure*}[t!]
%    \centering
%    \includegraphics[width=0.45\linewidth]{figures/moab-weights-computation.eps}
%    \caption{Ratio of weight computation to regridding phases in Blues and Cori}
%    \label{fig:regridding-mbtr-ratio}
%\end{figure*}
%%

\subsubsection{Strong Scalability of Spectral Projection (SE$\rightarrow$FV)}

To further evaluate the characteristics of in-memory remapping computation, along with cost of application of the weights during a transient simulation, a series of further studies were executed on the NERSC Cori system to determine the spectral projection of a real dataset between atmosphere and ocean components in E3SM. 
The source mesh contains 4th order spectral element temperature data defined on Gauss-Lobatto quadrature nodes (cGLL discretization) of the CS mesh, and the projection is performed on a MPAS polygonal mesh with holes (FV discretization). A direct comparison to ESMF was unfeasible in this study since the traditional workflow requires the computation of a dual mesh transformation of the spectral grid. Hence, only timings for MBTR workflow is shown here.

%The ocean meshes used for this study contain unstructured polygonal meshes only in the ocean regions and do not contain any elements in the land regions, while the atmosphere meshes covers the entire earth. 
Two specific cases were considered for this SE$\rightarrow$FV strong scaling study with conservation and monotonicity constraints. 
\begin{enumerate}
    \item \textbf{Case A (NE30):} 1-degree CS (30 edges per side) SE mesh (nele=5400 quads) with $p=4$ to MPAS mesh (nele=235160 polygons)
    \item \textbf{Case B (NE120):} 0.25-degree CS (120 edges per side) SE mesh (nele=86400 quads) with $p=4$ to MPAS mesh (nele=3693225 polygons)
\end{enumerate}

The performance tests for each of these cases were launched with three different process execution layouts for the atmosphere, ocean components and the coupler.
\begin{enumerate}[label=(\alph*)]
    \item Fully colocated PE layout: $N_{atm}=N_x$ and $N_{ocn}=N_x$
    \item Disjoint-ATM model PE layout: $N_{atm}=N_x/2$ and $N_{ocn}=N_x$
    \item Disjoint-OCN model PE layout: $N_{atm}=N_x$ and $N_{ocn}=N_x/2$
\end{enumerate}

\begin{table}[H]
    \caption{Strong scaling on Cori for SE$\rightarrow$FV projection with two different resolutions on a fully colocated PE layout}
    \begin{center}
        \begin{tabular}{|>{\centering\arraybackslash} m{2cm}|>{\centering\arraybackslash} m{2.2cm}|>{\centering\arraybackslash} m{2cm}|>{\centering\arraybackslash} m{2.2cm}|>{\centering\arraybackslash} m{2cm}|}
            \hline
            \multirow{2}{2cm}{\textbf{\parbox{\linewidth}{\vspace{0.8cm}}Number of processors}} & \multicolumn{2}{|p{4.3cm}|}{\centering \textbf{Case A (NE30)}} & \multicolumn{2}{|p{4.2cm}|}{\centering \textbf{Case B (NE120)}} \\
            \cline{2-5}
            & \textbf{Intersection (sec)} & \textbf{Compute Weights (sec)} & \textbf{Intersection (sec)} & \textbf{Compute Weights (sec)} \\
            \hline
            16 & 0.936846 & 0.64983 & 145.623 & 9.732 \\
            32 & 0.449022 & 0.429028 & 53.1244 & 5.78093 \\
            64 & 0.377767 & 0.373476 & 22.7167 & 4.92151 \\
            128 & 0.255154 & 0.270574 & 6.70485 & 2.79397 \\
            256 & 0.180136 & 0.18272 & 2.26435 & 1.71835 \\
            512 & 0.162388 & 0.104737 & 1.25471 & 0.928622 \\
            1024 & 0.203354 & 0.0932475 & 0.680122 & 0.618943 \\
            \hline
        \end{tabular}
    \end{center}
    \label{tbl:cori-ne30-ne120-scaling}
\end{table}

A breakdown of computational time for key tasks on Cori with up to 1024 processes for both the cases is tabulated in Table~\ref{tbl:cori-ne30-ne120-scaling} on a fully colocated decomposition i.e., $N_{ocn}=N_{atm}=N_x$. It is clear that the computation of parallel intersection mesh strong scales well for these production cases, especially for larger mesh resolutions (Case B). For the smaller source and target mesh resolution (Case A), we notice that the intersection time hits a lower bound that is dominated by the computation of the coverage mesh to enclose the target mesh in each task. It is important to stress that this one time setup call to compute remap operator, per component pair, is relatively much cheaper compared to individual component and solver initializations and get amortized over longer transient simulations. It is also worth noting that as the I/O bandwidth in emerging architectures are not scaling in line with the compute throughput, such an online workflow can generally be faster than parallel I/O for reading the weights from file at scale. The MBTR implementation is also flexible to allow loading the weights from file directly in order to preserve the existing coupler process with MCT.
%And since the remapping task is memory bandwidth , it offers the possibility to tackle very high resolution productions runs on next generation architectures.
%
In comparison to the computation of the intersection mesh, the time to assemble the remapping weight operator in parallel is generally smaller. Even though both of these operations are performed only once during the setup phase of the E3SM simulation, the weight operator computation involves several validation checks that utilize collective MPI operations, which do destroy the embarrassingly parallel nature of the calculation, once appropriate coverage mesh is determined in each task. 
% While it is not expected that this will prove to be a bottleneck on larger scale runs in production, optimizations can be implemented by collapsing the number of the collective calls and non-blocking reductions to improve performance.


The component-wise breakdown for the advancing front intersection mesh, the parallel communication graph for sending and receiving data between component and coupler, and finally, the remapping weight generation for the SE$\rightarrow$FV setup for NE30 and NE120 cases are shown in Fig.~\ref{fig:sscaling-cori}. The cumulative time for this remapping process is shown to scale linearly for NE120 case, even if the parallel efficiency decreases significantly in the NE30 case, as expected based on the results in Table~\ref{tbl:cori-ne30-ne120-scaling}. Note that the MBTR workflow provides a unique capability to consistently and accurately compute SE$\rightarrow$FV projection weights in parallel, without any need for an external pre-processing step to compute the dual mesh (as required by ESMF) or running the entire remapping process in serial (TempestRemap). 

Another key aspect of the results shown in Fig.~\ref{fig:sscaling-cori} is the relative indifference
in performance of the algorithms to the type of PE layout used to partition the component process space. Theoretically, we expect the fully disjoint case to perform the
worst, and a full colocated case with maximal overlap to perform the best, since the layout directly affects the total amount of data communicated for both the mesh and field data. However, in practice, with online repartitioning strategies exposed through Zoltan (PHG and RCB), overall scaling of the remapping algorithm is nearly independent of the PE layout for the simulation. This is especially evident from the timings for the coverage mesh computation
for the NE120 case for all three PE layouts.

%
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/Res12_Coverage_Intx_Weights.eps}
    \caption{Strong scaling study for the NE30 and NE120 cases for spectral projection with Zoltan repartitioner on Cori}
    \label{fig:sscaling-cori}
\end{figure}
%

%\begin{figure*}[t!]
%    \centering
%    \begin{subfigure}[t]{0.98\linewidth}
%        \centering
%        \includegraphics[width=\textwidth]{figures/res1_cov_intx_wgts.eps}
%        \caption{NE30 component-wise strong scaling}
%        \label{fig:sscaling-breakdown-cori-ne120-a}
%    \end{subfigure}
%    \begin{subfigure}[t]{0.98\linewidth}
%        \centering
%        \includegraphics[width=\textwidth]{figures/res2_cov_intx_wgts.eps}
%        \caption{NE120 component-wise strong scaling}
%        \label{fig:sscaling-breakdown-cori-ne120-b}
%    \end{subfigure}
%    \caption{Test stitch}
%    \label{fig:test}
%\end{figure*}



%
%%
%\begin{figure*}[t!]
%    \centering
%%    \begin{subfigure}[t]{0.3\linewidth}
%%        \centering
%%        \includegraphics[width=\textwidth]{figures/res2_colocated_coveragemesh.eps}
%%        \caption{Coverage mesh computation}
%%        \label{fig:sscaling-breakdown-cori-ne120-a}
%%    \end{subfigure}
%    \begin{subfigure}[t]{0.3\linewidth}
%        \centering
%        \includegraphics[width=\textwidth]{figures/res2_colocated_intersection.eps}
%        \caption{Advancing front intersection computation}
%        \label{fig:sscaling-breakdown-cori-ne120-b}
%    \end{subfigure}
%    \begin{subfigure}[t]{0.3\linewidth}
%        \centering
%        \includegraphics[width=\textwidth]{figures/res2_colocated_computeweights.eps}
%        \caption{Weight operator assembly with TempestRemap}
%        \label{fig:sscaling-breakdown-cori-ne120-c}
%    \end{subfigure}
%%    \begin{subfigure}[t]{0.3\linewidth}
%%        \centering
%%        \includegraphics[width=\textwidth]{figures/res2_colocated_sndrcvdata.eps}
%%        \caption{Send/Receive data between $N_x$ and $N_{c,l}$}
%%        \label{fig:sscaling-breakdown-cori-ne120-d}
%%    \end{subfigure}
%    \begin{subfigure}[t]{0.3\linewidth}
%        \centering
%        \includegraphics[width=\textwidth]{figures/res2_colocated_applywgts.eps}
%        \caption{Weight operator application}
%        \label{fig:sscaling-breakdown-cori-ne120-e}
%    \end{subfigure}
%    \caption{Strong scaling of the per-process remapping phases for the NE120 case}
%    \label{fig:sscaling-breakdown-cori}
%\end{figure*}
%%


\subsection{Effect of partitioning strategy}

In order to determine the effect of partitioning strategies described in Fig.~\ref{fig:mpas-partitioning-strategy}, the NE120 case with the trivial decomposition and Zoltan geometric partitioner (RCB) were tested in parallel. Fig.~\ref{fig:partitioner-comparison-scaling} compares the two strategies for optimizing the mesh migration from the component to coupler. These strategies play a critical role in task mapping and data locality for the source coverage mesh computation, in addition to determining the communication graph complexity between the components and the coupler. 
This comparison highlights that the coverage mesh cost reduces uniformly at scale, while the trivial partitioning scheme behaves better on lower core counts as shown in Fig.~\ref{fig:partitioner-comparison-scaling}-(a). The communication of field data between the atmosphere component and the coupler resulting from the partitioning strategy is a critical operation during the transient simulation, and generally stays within network latency limits in Cori (shown in Fig.~\ref{fig:partitioner-comparison-scaling}-(b)). Eventhough the communication kernel does not show ideal scaling on increasing node counts, the relative cost of the operation should be insignificant in comparison to total time spent in individual component solvers. Note that production climate model solvers require multiple data fields to be remapped at every rendezvous timestep, and hence the size of the packed messages may be larger for such simulations. We also note that there is a factor of 3 increase in the communication time to send and receive data, which occurs after the 64 process count on Cori in Fig.~\ref{fig:partitioner-comparison-scaling}-(b). This is an artifact of the additional communication latency due to the transition from an intra-node (each Haswell node in Cori accommodates 64 processes) to inter-node nearest neighbor data transfer when using multiple nodes. In this strong scaling study, the net data size being transferred reduces with increasing core counts, and hence the point-to-point communication beyond 128 processes is primarily dominated by network latency and task placement. As part of future extensions, we will further explore task mapping strategies with the Zoltan2 \citep{zoltan2} library, in addition to online partition rebalancing to maximize geometric overlap, and minimize communication time during remapping. %Besides this surge, the plateauing of communication time as the message size decreases with increasing process count should . 

%
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/Scalability-Coverage-SendReceive.eps}
    \caption{Scaling of the communication kernels driven with the parallel graph computed with a trivial redistribution and the Zoltan geometric (RCB) repartitioner for the NE120 case with $N_{ocn}=N_x$ and $N_{atm}=N_x/2$ on Cori}
    \label{fig:partitioner-comparison-scaling}
\end{figure}
%

\subsection{Note on Application of Weights}

Generally, operations involving Sparse Matrix-Vector (SpMV) products are memory bandwidth limited \citep{bell2009}, and occur during the application of remapping weights operator on to the source solution field vector, in order to compute the field projection onto the target grid.
In addition to the communication of field data shown in Fig.~\ref{fig:partitioner-comparison-scaling}-(b), the cost of remapping weight application in parallel (presented in Fig.~\ref{fig:sscaling-breakdown-cori-ne120-applywgts}) determines the total cost of the remapping operation during runtime. Except for the case of cGLL target discretizations, the parallel SpMV operation during the weight application do not involve any global collective reductions. In the current E3SM and OASIS3-MCT workflow, these operations are handled by the MCT library.
In high resolution simulations of E3SM, the total time for the remapping operation in MCT is primarily dominated by the communication costs based on the communication graph, similar to the MBTR workflow. 
However, a direct comparison of the communication kernels in these two workflows is not yet possible, since the offline maps for MCT that are generated with ESMF use the "dual" grid, while the online maps generated with MBTR utilize the original spectral grid with no approximations, which results in very different communication graph and non-zero pattern in the remap weight matrices. 
%but we expect the aggregated communication strategies in the crystal router algorithm \citep{fox1989} in MOAB, to provide relatively better performance at scale.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/res2_colocated_applywgts.eps}
    \caption{SE$\rightarrow$FV remapping weight operator application for the NE120 case on Cori}
    \label{fig:sscaling-breakdown-cori-ne120-applywgts}
\end{figure}


%\subsection*{Effect of partitioning scheme}
%
%Use trivial partition vs Zoltan partitioner for data migration 

% How does master/slave partitioner improve performance; implications for the workflow and impact of prepartitioning


