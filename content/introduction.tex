\section{Introduction}

% Why is remapping critical to preserve higher-order accuracy and stability in modeling complex coupled multi-physics climate systems 

Understanding Earth's climate evolution through robust and accurate modeling 
of the intrinsically complex, coupled ocean-atmosphere-land-ice-biosphere models 
requires extreme-scale computational power \citep{washington_2008}. 
In such coupled applications, the different component models may employ 
unstructured spatial meshes that are specifically generated to resolve 
problem-dependent solution variations, which introduces several challenges in 
performing a consistent solution coupling. It is known that operator decomposition 
and unresolved coupling errors in partitioned atmosphere and ocean model simulations \citep{beljaars_2017}, 
or physics and dynamics components of an atmosphere, can lead to large 
approximation errors that cause severe numerical stability issues. In this context, one factor contributing to the spatiotemporal accuracy is the mapping between different discretizations of the sphere used in the 
components of a coupled climate model.  Accurate remapping strategies in 
such multi-mesh problems are critical to preserve higher order resolution, 
but are in general computationally expensive given the disparate spatial scales 
across which conservative projections are calculated. Since the primal solution 
or auxiliary derived data defined on a donor physics component mesh (source model) needs to 
be transferred to its coupled dependent physics mesh (target model), robust 
numerical algorithms are necessary to preserve discretization accuracy during 
these operations \citep{grandy_1999, DeBoer2008}, in addition to conservation and monotonicity 
properties in the field profile. 

An important consideration is that in addition to maintaining the overall discretization 
accuracy of the solution during remapping, global conservation,
and sometimes local element-wise conservation for critical quantities \citep{jiao_heath_2004} 
needs to be imposed during the workflow. Such stringent requirements on 
key flux fields that couple components along boundary interfaces is necessary in 
order to mitigate any numerical deviations in coupled climate simulations. Note that these 
physics meshes are usually never embedded or are not linked by any trivial linear transformations, which
render existence of exact projection or interpolation operators unfeasible, even if the
same continuous geometric topology is discretized in the models. 
Additionally, the unique domain decomposition used for each of 
the component physics meshes complicates the communication pattern during 
intra-physics transfer, since aggregation of point location requests needs to be handled
efficiently in order to reduce overheads during the remapping workflow \citep{Plimpton2004, tautges_scalable_2009}.

Adaptive block-structured cubed-sphere or unstructured refinement of icosahedral/polygonal meshes \citep{slingo_2009} are often used to resolve the complex fluid dynamics behavior in atmosphere and ocean models efficiently. 
In such models, conservative, local flux-preserving remapping schemes are critically important 
\citep{berger1987} to effectively reduce multimesh errors, especially during computation of tracer advection such as water vapor or $CO_2$ \citep{lauritzen_2010}. 
This is also an issue in atmosphere models where physics and dynamics
are computed on non-embedded grids \citep{dennis_2012}, and the improper spatial coupling between these multi-scale models could introduce numerical artifacts.  
Hence, the availability of different consistent and accurate
remapping schemes under one flexible climate simulation framework is vital to better 
understand the pros and cons of the adaptive multiresolution choices \citep{reichler_2008}.
%
%In some atmospheric and ocean model solvers, consistent and conservative remapping strategies strongly determine the propagation of discretization accuracy between models. 

%
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.52]{figures/e3sm-old-new-workflow.eps}
    \caption{E3SM Coupled Climate Solver: (a) Current model (left), (b) Newer MOAB based coupler (right).}
    \label{fig:e3sm-workflow-oldnew}
\end{figure}
%

\subsection{Hub-and-Spoke vs Distributed Coupling Workflow}
\label{subsec:remapping-workflow}

The hub-and-spoke centralized model as shown in Fig.~\ref{fig:e3sm-workflow-oldnew} (left) is used in the current Exascale Earth System Model (\links{https://www.e3sm.org}{E3SM}) driver, and relies on several 
tools and libraries that have been developed to simplify the regridding workflow within the 
climate community. Most of the current tools used in E3SM and the Community Earth System Model (CESM)  \citep{hurrell2013} are included in a single package called the Common Infrastructure for Modeling the Earth (\links{http://esmci.github.io/cime}{CIME}), which builds on previous couplers used in CESM \citep{craig2005,craig2012}.  
These modeling tools approach the problem in a two-step computational process: 

\begin{enumerate}
\item Compute the projection or remapping weights for a solution field 
from a source component physics to a target component physics as an \underline{offline process}
\item During runtime, the CIME coupled solver loads the remapping weights from a file, and handles the 
partition-aware \underline{communication and weight matrix} application to project coupled fields between components
\end{enumerate} 

The first task in this workflow is currently accomplished through a variety of standard state-of-science tools such as the Earth Science Modeling Framework (ESMF) \citep{esmf_hill_2004}, Spherical Coordinate Remapping and Interpolation Package (SCRIP) \citep{Jones1999}, TempestRemap \citep{ullrich_2013, ullrich2015}. 
The Model Coupling Toolkit (MCT) \citep{mct_larson2001,jacob2005m} used in the CIME solver provides 
data structures for the second part of the workflow.
Traditionally the first workflow phase is executed decoupled from the simulation driver during a pre-processing step, and hence any updates to the field discretization or the underlying mesh resolution immediately necessitates recomputation of the remapping weight generation workflow with updated inputs. This process flow also prohibits the component solvers from performing any runtime spatial adaptivity, since the remapping weights have to be re-computed dynamically after any changes in grid positions. To overcome such deficiencies, and to accelerate the current coupling workflow, recent efforts have been undertaken to implement a fully integrated remapping weight generation process within E3SM using a scalable infrastructure provided by the topology, decomposition and data-aware Mesh Oriented datABase (\links{http://sigma.mcs.anl.gov/moab-library}{MOAB}) \citep{moab_tautges_2004, sigma_v12_osti_1224985} and TempestRemap \citep{ullrich_2013} software libraries as shown in Fig.~\ref{fig:e3sm-workflow-oldnew} (right). Note that whether a hub-and-spoke or distributed coupling model is used to drive the simulation, a minimal layer of driver logic is necessary to compute weighted combination of fluxes, validation metrics, and other diagnostic outputs.

The paper is organized as follows. In Section.~(\ref{sec:background}), we present the necessary background and motivations to develop an online remapping workflow implementation in E3SM. Section.~(\ref{sec:algorithms}) covers details on the scalable, mesh and partition aware, conservative remapping algorithmic implementation to improve
scientific productivity of the climate scientists, and to simplify the overall computational workflow for complex problem simulations. Then, the performance of these algorithms are first evaluated in serial for various grid combinations, and the parallel scalability of the workflow is demonstrated on large-scale machines in Section.~(\ref{sec:results}). 
%This is accomplished through the usage of a mesh, decomposition and data aware software interfaces 
%to the MOAB while reusing the verified and validated weight generation numerics in TempestRemap.
