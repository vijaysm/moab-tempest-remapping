\section{Algorithmic approach}
\label{sec:algorithms}

%\needalgorithmstrue
\needalgorithmsfalse

Efficient, conservative and accurate multi-mesh solution transfer workflows \citep{mct_jacob_2005, tautges_scalable_2009} are a complex process. This is due to the fact that in order to ensure conservation of critical quantities in a given norm, exact cell intersections between the source and target grids have to be computed. This is complicated in a parallel setting since the domain decompositions between the source and target grids may not have any overlaps, making it a potentially all-to-all collective communication problem. Hence, efficient implementations of regridding operators need to be mesh, resolution, field and decomposition aware in order to provide optimal performance in emerging architectures. 

Fully online remapping capability within a complex ecosystem such as E3SM requires a flexible infrastructure to generate the projection weights. In order to fullfill these needs, we utilize the MOAB mesh datastructure combined with the TempestRemap libraries in order to provide an in-memory remapping layer to dynamically compute the weight matrices during the setup phase of the simulations for static source-target grid combinations. For dynamically adaptive and moving grids, the remapping operator can be recomputed at runtime as needed. The introduction of such a software stack allows higher order conservation of fields while being able to transfer and maintain field relations in parallel, within the context of the fully decomposed mesh view. This is an improvement to the E3SM workflow where MCT is oblivious to the underlying mesh datastructure in the component models. Having a fully mesh-aware implementation with element connectivity and adjacency information, along with parallel ghosting and decomposition information also provides opportunities to implement dynamic load-balancing algorithms to gain optimal performance on large-scale machines. Without the mesh topology, MCT is limited to performing trivial decompositions based on global ID spaces during mesh migration operations. YAC interpolator \citep{yac_2016} and the multidimensional Common Remapping software (CoR) in C-Coupler2 \citep{ccoupler2_2018} provide similar capabilities to perform a parallel tree-based search for point location and interpolation through various supported numerical schemes. 

MOAB is a fully distributed, compact, array-based mesh datastructure, and the local entity lists are stored in ranges along with connectivity and ownership information, rather than explicit lists, thereby leading to a high degree of memory compression. The memory constraints per process scales well in parallel \citep{tautges_scalable_2009}, and is only proportional to the number of entities in the local partition, which reduces as number of processes increases (strong scaling limit). This is similar to the Global Segment Map (GSMap) in MCT, which in contrast is stored in every processor, leading to $O(N_x)$ memory requirements. The parallel communication infrastructure in MOAB is heavily leveraged \citep{tautges_jason_2012} to utilize the scalable, {\em crystal router algorithm} \citep{fox1989, schliephake-2015} in order to scalably communicate the covering cells to different processors. This parallel mesh infrastructure in MOAB provides the necessary algorithmic tools for optimally executing online remapping strategies, so that MCT in E3SM can be replaced with a MOAB-based coupler.

In order to illustrate the online remapping algorithm implemented with the MOAB-TempestRemap infrastructure, we define the following terms. Let $N_{c,S}$ be the component processes for source mesh, $N_{c,T}$ be the component processes for target mesh and $N_x$ be the coupler processes where the remapping operator is computed. More generally, the problem statement can be defined as: transfer a solution field $U$ defined on the domain $\Omega_S$  and processes $N_{c,S}$, to the domain $\Omega_T$ and processes $N_{c,T}$, through a centralized coupler with domain information $\Omega_{S} \bigcup \Omega_{T}$ defined on $N_x$ processes.
Such a complex online remapping workflow for projecting the field data from a source to target mesh follows the algorithm shown in Algorithm. \ref{alg:workflow-remapping}.

\ifneedalgorithms
%    \BlankLine
%%
\begin{algorithm}
    \caption{MOAB-TempestRemap parallel regridding workflow}
    \label{alg:workflow-remapping}
    \begin{algorithmic}[1]
        \State \textbf{Input}: Partitioned and distributed native component meshes on $N_{c,S}$ source and $N_{c,T}$ target processes
        \State \textbf{Result}: Remapping weight matrix $W_{S \rightarrow T}$ computed for a source ($S$) and target ($T$) mesh pair on $N_x$ coupler processes

        \Statex
        \State \textbf{\underline{Scope:}} Coupler $N_x \gets$ component mesh $N_{c,l}$, where $l \in [S,T]$
            \For{ each component $l \in [S,T]$}
                \State -- \textbf{create in-memory copy} of component unstructured mesh and data using MOAB interfaces (Section. \ref{ssec:e3sm-interface})
                \State -- \textbf{migrate} MOAB component mesh to coupler; repartition from $N_{c,l} \rightarrow N_x$ (Section. \ref{ssec:mesh-migration})
            \EndFor

        \Statex
        \State \textbf{\underline{Scope:}} Compute pair-wise intersection mesh on coupler processes $N_x$
            \For{ each mesh pair to be regridded: $\Omega_S$ and $\Omega_T$ in $N_x$ }
                \If{$(\Omega_T - \Omega_T \cap \Omega_S) \neq 0$}\Comment{local source mesh does not cover target mesh}
                    \State collectively gather coverage mesh $\Omega_{Sc}$ on $N_x$ $\mid$ $(\Omega_T - \Omega_T \cap \Omega_{Sc}) = 0$ (Section. \ref{ssec:coverage-mesh})
                \EndIf
                \State -- \textbf{store communication graph} to send/receive between $N_{c,l}$ and $N_x$
                \State -- \textbf{compute $\Omega_{ST} = \Omega_{Sc} \cap \Omega_{T}$} through an \emph{advancing-front algorithm} \citep{Lohner_1988, Gander2009} (Section. \ref{sec:adfront-algo})
                \State -- \textbf{evaluate source/target element mapping} for $e_i \in \Omega_{ST}$
                \State -- \textbf{exchange ghost cell information} for $\Omega_{ST}$
            \EndFor

        \Statex
        \State \textbf{\underline{Scope:}} Integrate over $\Omega_{ST}$ to compute remapping weights
            \For{ each intersection polygon element $e_i \in \Omega_{ST}$ }
                \State -- \textbf{Tessellate $e_i$} into triangular elements with reproducible ordering
                \State -- \textbf{Compute projection integral} with consistent Triangular quadrature rules
                \State -- \textbf{Determine row/col DoF coupling} through $e_i$ parent association to $\Omega_S/\Omega_T$ 
                \State -- \textbf{Assemble local matrix weights} such that $W_{S \rightarrow T} = \sum_{1}^{N_x} w_{ij}$, where $w_{ij}$ represents the coupling between local target DoF (row $i$) and source DoF (col $j$) in projection operator (Section. \ref{ssec:remap-tempest})
            \EndFor

    \end{algorithmic}
\end{algorithm}
%

\else

%%
%\begin{figure*}[t!]
%    \centering
%    \includegraphics[width=\textwidth]{figures/Online-Remapping-Algorithm.eps}
%    \caption{MOAB-TempestRemap parallel regridding workflow.}
%    \label{alg:workflow-remapping}
%\end{figure*}
%%
%    \BlankLine
%%
\begin{algorithm}
    \caption{MOAB-TempestRemap parallel regridding workflow}
    \label{alg:workflow-remapping}
    \begin{algorithmic}[1]
        \STATE \textbf{Input}: Partitioned and distributed native component meshes on $N_{c,S}$ source and $N_{c,T}$ target processes
        \STATE \textbf{Result}: Remapping weight matrix $W_{S \rightarrow T}$ computed for a source ($S$) and target ($T$) mesh pair on $N_x$ coupler processes
        
        \item[]
        \STATE \textbf{\underline{Scope:}} Coupler $N_x \gets$ component mesh $N_{c,l}$, where $l \in [S,T]$
        \FOR{ each component $l \in [S,T]$}
        \STATE -- \textbf{create in-memory copy} of component unstructured mesh and data using MOAB interfaces (Section. \ref{ssec:e3sm-interface})
        \STATE -- \textbf{migrate} MOAB component mesh to coupler; repartition from $N_{c,l} \rightarrow N_x$ (Section. \ref{ssec:mesh-migration})
        \ENDFOR

        \item[]
        \STATE \textbf{\underline{Scope:}} Compute pair-wise intersection mesh on coupler processes $N_x$
        \FOR{ each mesh pair to be regridded: $\Omega_S$ and $\Omega_T$ in $N_x$ }
        \STATE \textbf{Ensure:} \COMMENT{local source mesh fully covers target mesh}
        \IF{$(\Omega_T - \Omega_T \cap \Omega_S) \neq 0$} %\COMMENT{local source mesh does not cover target mesh}
        \STATE collectively gather coverage mesh $\Omega_{Sc}$ on $N_x$ $\mid$ $(\Omega_T - \Omega_T \cap \Omega_{Sc}) = 0$ (Section. \ref{ssec:coverage-mesh})
        \ENDIF
        \STATE -- \textbf{store communication graph} to send/receive between $N_{c,l}$ and $N_x$
        \STATE -- \textbf{compute $\Omega_{ST} = \Omega_{Sc} \cap \Omega_{T}$} through an \emph{advancing-front algorithm} \citep{Lohner_1988, Gander2009} (Section. \ref{sec:adfront-algo})
        \STATE -- \textbf{evaluate source/target element mapping} for $e_i \in \Omega_{ST}$
        \STATE -- \textbf{exchange ghost cell information} for $\Omega_{ST}$
        \ENDFOR

        \item[]
        \STATE \textbf{\underline{Scope:}} Integrate over $\Omega_{ST}$ to compute remapping weights
        \FOR{ each intersection polygon element $e_i \in \Omega_{ST}$ }
        \STATE -- \textbf{Tessellate $e_i$} into triangular elements with reproducible ordering
        \STATE -- \textbf{Compute projection integral} with consistent Triangular quadrature rules
        \STATE -- \textbf{Determine row/col DoF coupling} through $e_i$ parent association to $\Omega_S/\Omega_T$ 
        \STATE -- \textbf{Assemble local matrix weights} such that $W_{S \rightarrow T} = \sum_{1}^{N_x} w_{ij}$, where $w_{ij}$ represents the coupling between local target DoF (row $i$) and source DoF (col $j$) in projection operator (Section. \ref{ssec:remap-tempest})
        \ENDFOR
        
    \end{algorithmic}
\end{algorithm}
%

\fi

%
%Kd-tree based point location with bounding box computation; MOAB is different that it uses an adaptive version of the tree construction \citep{tautges_scalable_2009, tautges_jason_2012}
%
%Current MOAB implementation uses an advancing front algorithm \citep{Lohner_1988}; very different in comparison to methods in ESMF/DTK and Portage
%}

In the following sections, the new E3SM online remapping interface implemented with a combination of the MOAB and TempestRemap libraries is explained. Details regarding the algorithmic aspects to compute conservative, high-order remapping weights in parallel, without sacrificing discretization accuracy on next generation hardware are presented.

\subsection{Interfacing to Component Models in E3SM}
\label{ssec:e3sm-interface}

Within the E3SM simulation ecosystem, there are multiple component models (atmosphere-ocean-land-ice-runoff) that are coupled to each other. While the MCT infrastructure primarily manages the global Degree-of-Freedom (DoF) partitions without a notion of the underlying mesh, the new MOAB-based coupler infrastructure provides the ability to natively interface to the component mesh, and intricately understand the field DoF data layout associated with each model.  MOAB can recognize the difference between values on a cell center and values on a cell edge or corner. In the current work, the MOAB mesh database has been used to create the relevant integration abstraction for the HOMME atmosphere model \citep{thomas2005, homme_taylor_2007}  (cubed-sphere SE grid) and the Model for Prediction Across Scales (MPAS) ocean model \citep{ringler2013, petersen2015} (polygonal meshes with holes representing land and ice regions). Since details of the mesh are not available at the level of the coupler interface, additional MOAB (Fortran) calls via the \texttt{iMOAB} interface are added to HOMME and MPAS component models to describe the details of the unstructured mesh to MOAB with explicit vertex and element connectivity information, in contrast to MCT coupler that is oblivious to the underlying grid. The atmosphere-ocean coupling requires the largest computational effort in the coupler (since they cover about 70\% of the coupled domain), and hence bulk of discussions in the current work will focus on remapping and coupling between these two component models. 

\setcounter{footnote}{0}

MOAB can handle the finite-element zoo of elements on a sphere (triangles, quadrangles, and polygons) making it an appropriate layer to store both the mesh layout (vertices, elements, connectivity, adjacencies) and the parallel decomposition for the component models along with information on shared and ghosted entities. While having a uniform partitioning methodology across components may be advantageous for improving the efficiency of coupled climate simulations, the parallel partition of the meshes are chosen according to the requirements in individual component solvers. Fig.~\ref{fig:homme-mpas-moab} shows examples of partitioned SE and MPAS meshes, visualized through the native MOAB plugin for \links{https://visit.llnl.gov}{VisIt} \citep{visit_2005}.

%
\begin{figure*}[t!]
    \centering
    \includegraphics[width=\textwidth]{figures/HOMME-MPAS-Mesh.eps}
    \caption{MOAB representation of partitioned component meshes.}
    \label{fig:homme-mpas-moab}
\end{figure*}
%

The coupled field data that is to be remapped from the source grid to the target grid also needs to be serialized as part of the MOAB mesh database in terms of an internally contiguous, MOAB data storage structure named a `Tag' \citep{moab_tautges_2004}.
For E3SM, we use element-based tags to store the partitioned field data that is required to be remapped between components. Typically, the number of DoF per element ($nDoF_e$) is determined based on the underlying discretization; $nDoF_e=p^2$ values in HOMME where $p$ is the order of SE discretization, 
and $nDoF_e=1$ for the FV discretization in MPAS ocean. With this complete description of the mesh and associated data for each component model, 
MOAB contains the necessary information to proceed with the remapping workflow.


\subsection{Migration of Component Mesh to Coupler}
\label{ssec:mesh-migration}

E3SM's driver supports multiple modes of partitioning the various components in the global processor space. This is usually fine tuned based on the estimated computational load in each physics, according to the problem case definition. A sample process-execution (PE) layout for a E3SM run on 9000 processes with ATM on 5400 and OCN on 3600 processes is shown in Fig.~\ref{fig:pelayout-e3sm}.
% 
In the case shown in the schematic, $N_{c,ATM}=5400$, $N_{c,OCN}=3600$ and $N_{x}=4800$. In such a PE layout, the atmosphere component mesh from HOMME, distributed on $N_{c,ATM}$ (5400) processes needs to be migrated and redistributed on $N_{x}$ (4800) processes. Similarly, from $N_{c,OCN}$ (3600) to $N_{x}$ (4800) processes for the MPAS ocean mesh. 
%During the online remapping process, such a variable partition layout implies that a one-to-one mapping to transfer the data between the component and the coupler processes in order to transfer the data directly. 
In the hub-and-spoke coupling model as shown in Fig.~\ref{fig:e3sm-workflow-oldnew}, the remapping computation is performed only in the coupler processors. Hence, inference of a communication pattern becomes necessary to ensure scalable data transfers between the components and the coupler. In the existing implementation, MCT handles such communication, which is being replaced by point-to-point communication kernels in MOAB to transfer mesh and data between different components or component-coupler PEs. Note that in a distributed coupler, source and target components can communicate directly, without any intermediate transfers (through the coupler). Under the unified infrastructure provided by MOAB, minimal changes are required to enable either the hub-and-spoke or the distributed coupler for E3SM runs, which offers opportunities to minimize time to solution without any changes in spatial coupling behavior.

%
\begin{figure*}[t!]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/pelayout_e3sm.eps}
    \caption{Example E3SM process execution layout for a problem case}
    \label{fig:pelayout-e3sm}
\end{figure*}
%

For illustration, let $N_{c}$ be the number of component process elements, and $N_{x}$ be the number of coupler process elements. In order to migrate the mesh and associated data from $N_{c}$ to $N_{x}$, we first compute a trivial partition of elements that map directly in the partition space, the
same partitioning as used in the CIME-MCT coupler. In MOAB, we have exposed parallel graph and geometric repartitioning schemes through interfaces to
Zoltan \citep{zoltan2002} or ParMetis \citep{parmetis1997}, in order to evaluate optimized migration patterns to minimize the volume of data communicated between component and coupler. We intend to analyze the impact of different migration schemes on the scalability of the remapping operation in Section.~(\ref{sec:results}). These optimizations have the potential to minimize data movement in the MOAB-based remapper, and to make it a competitive data broker to replace the current MCT \citep{jacob2005m} coupler in E3SM.

%
\begin{figure*}[t!]
    \centering
    \includegraphics[width=\textwidth]{figures/Migration-Trivial-Zoltan.eps}
    \caption{Migration strategies to repartition from $N_{c} \rightarrow N_{x}$}
    \label{fig:mpas-partitioning-strategy}
\end{figure*}
%

We show an example of a decomposed ocean mesh (polygonal MPAS mesh) that is replicated in a E3SM problem case run on two processes in Fig.~\ref{fig:mpas-partitioning-strategy}. Fig.~\ref{fig:mpas-partitioning-strategy}-(a) is the original decomposed mesh on 2 processes $\in N_{c}$, while Fig.~\ref{fig:mpas-partitioning-strategy}-(b) and Fig.~\ref{fig:mpas-partitioning-strategy}-(c) show the impact of migrating a mesh from 2 $N_{c}$ processes to 4 processes $\in N_{x}$ with a trivial linear partitioner and a Zoltan based geometric online partitioner. The decomposition in Fig.~\ref{fig:mpas-partitioning-strategy}-(b) shows that the element ID based linear partitioner can produce bad data locality, which may require large number of nearest neighbor communications when computing a source coverage mesh. The resulting communication pattern can also make the migration, and coverage computation process non-scalable on larger core counts. In contrast, in Fig.~\ref{fig:mpas-partitioning-strategy}-(c),  the Zoltan partitioners produce much better load balanced decompositions with Hypergraph (PHG), Recursive Coordinate Bisection (RCB) or Recursive Inertial Bisection (RIB) algorithms to reduce communication overheads in the remapping workflow. In order to better understand the impact of online decomposition strategies on the overall remapping operation, we need to better understand the impact of the repartitioner on two communication-heavy steps.
\begin{enumerate}
    \item Mesh migration from component to coupler involving communication between $N_{c,s/t}$ and $N_x$,
    \item Computing the coverage mesh requiring gather/scatter of source mesh elements to cover local target elements.
\end{enumerate}
In a hub-and-spoke model with online remapping, the best coupler strategy will require a simultaneous partition optimization for all grids such that mesh migration includes constraints on geometric coordinates of component pairs. While such extensions can be implemented within the infrastructure presented here, the performance discussions in Section \ref{sec:results} will only focus on the trivial and Zoltan-based partitioners. It is also worth noting that in a distributed coupler, pair-wise migration optimizations can be performed seamlessly using a master(\textit{target})-slave(\textit{source}) strategy to maximize partition overlaps.

% \comment{talk about intersection optimization by using master-slave decompositions}

%\subsubsection*{Parallel communication graph computation}

%How do we compute the comm graph ? An illustration may be better. Iulian ?

\subsection{Computing the Regridding Operator}
\label{ssec:compute-regrid}

Standard approaches to compute the intersection of two convex polygonal meshes involve the creation of a Kd-tree \citep{kdtree_hunt2006} or BVH-tree datastructure \citep{bvhtree_ize2007} to enable fast element location of relevant target points. In general, each target point of interest is located on the source mesh by querying the tree datastructure, and the corresponding (source) element is then marked as a contributor to the remapping weight computation of the target DoF. This process is repeated to form a list of source elements that interact directly according to the consistent discretization basis. TempestRemap, ESMF and YAC use variations of this search-and-clip strategy tailored to their underlying mesh representations. 

%\subsubsection{Local (Serial) Intersection Method for Overlapping Meshes}
\subsubsection{Advancing Front Intersection -- A Linear Complexity Algorithm}
\label{sec:adfront-algo}

The intersection algorithm used in this paper follows the ideas from \citep{Lohner_1988, Gander2013}, in which two meshes are covering the same domain. At the core is an advancing front method that aims to traverse through the source and target meshes to compute a union (super) mesh. First, two convex cells from the source coverage mesh and the target meshes that intersect are identified by using an adaptive Kd-tree search tree datastructure. This process also includes determination of the seed (the starting cell) for the advancing front in each of the partitions independently. Advancing in both meshes using face adjacency information, incrementally all possible intersections are computed \citep{Brezina2017} accurately to a user defined tolerance (default = $1e-15$) in linear time.
%Important for the algorithm is also a robust scheme, in which two intersecting cells from the different meshes are overlapped and resolved accurately.

While the advancing front algorithm is not restricted to convex cells, the intersection computation is simpler if they are strictly convex. If concave polygons exist in the initial source or target meshes, they are recursively decomposed into simpler convex polygons, by splitting along interior diagonals. Note that the intersection between two convex polygons results in a strictly convex polygon. Hence, the underlying intersection algorithm remains robust to resolve even arbitrary non-convex meshes covering the same domain space.

%
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/adfront-illustration.eps}
    \caption{Illustration of the advancing front intersection algorithm.}
    \label{fig:adfront-illustration}
\end{figure}
%

Fig.~\ref{fig:adfront-illustration} illustrates how the algorithm advances. In each local partition of the coupler PEs, a pair of source (blue) and target (red) cells that intersect is found Fig.~\ref{fig:adfront-illustration} (A). Using face adjacency queries for the source mesh, as shown in Fig.~\ref{fig:adfront-illustration} (B), all source cells  that intersects the current target cell are found.  New possible pairs between cells adjacent to the current target cell and other source cells are added to a local queue. After the current target cell is resolved, a pair from the queue is considered next.  Fig.~\ref{fig:adfront-illustration} (C) shows the resolving of a second target cell , which is intersecting here with 3 source cells. If both meshes are contiguous, this algorithm guarantees to compute all possible intersections between source and target cells. Fig.~\ref{fig:adfront-illustration} (D) shows the colormap representation of the progression in which the intersection polygons were found, from blue (low count) towards red . This advance/progression is also illustrated through videos in both the serial \citep{Mahadevan2018-af1} and parallel \citep{Mahadevan2018-af2} contexts on partitioned meshes.

This flooding-like advancing front needs a stable and robust methodology of intersecting edges/segments in two cells that belong to different meshes. Any pair of segments that intersect can appear in four different pairs of cells. A list of intersection points is maintained on each target edge, so that the intersection points are unique. Also, a geometric tolerance is used to merge intersection points that are close to each other, or if they are proximal to the original vertices in both meshes. Decisions regarding whether points are inside, outside or at the boundary of a convex enclosure are handled separately. If necessary, more robust techniques such as adaptive precision arithmetic procedures used in \emph{Triangle} \citep{shewchuk1996}, can be employed to resolve the fronts more accurately.
Note that the advancing front strategy can be employed for meshes with topological holes (e.g. ocean meshes, in which the continents are excluded) without any further modifications by using a new pair for each disconnected region in the target mesh.


%Each target cell is resolved by building a local queue of source cells that intersect the target cell. Source cells are added to a local queue incrementally, using adjacency information. At the same time, a global queue with seeds is formed, containing pairs of source/target cells that have the probability to intersect. When there are no more source cells in the local queue, the algorithm advances to the next seed from the global queue, and the algorithm repeats. 


\subsubsection*{Note on Gnomonic Projection for Spherical Geometry}
\label{ssec:gnomonic-projection}

Meshes that appear in climate applications are often on a sphere. Cell edges are considered to be great circle arcs. A simple gnomonic projection is used to project the edges on one of the six planes parallel to the coordinate axis, and tangent to the sphere \citep{ullrich_2013}. With this projection, all curvilinear cells on the sphere are transformed to linear polygons on a gnomonic plane, which simplifies the computation of intersection between multiple grids. Once the intersection points and cells are computed on the gnomonic plane, these are projected back on to the original spherical domain without approximations. This is possible due to the fact that intersection can be computed to machine precision as the edges become straight lines in a gnomonic plane (projected from great circle arcs on a sphere). If curves on a sphere are not great circle arcs (splines, for example), the intersection between those curves have to be computed using some nonlinear iterative procedures such as Newton Raphson (depending on the representation of the curves). 

\subsection{Parallel Implementation Considerations}
\label{ssec:parallel-considerations}

Existing Infrastructure from MOAB \citep{moab_tautges_2004} was used to extend the advancing front algorithm in parallel. The expensive intersection computation can be carried out independently, in parallel, once we redistribute the source mesh to envelope the target mesh areas fully, in a step we refer to as `source coverage mesh' computation. 

\subsubsection{Computation of a Source Coverage Mesh}
\label{ssec:coverage-mesh}

We select the target mesh as the driver for redistribution of the source mesh. On each task, we first compute the bounding box of the local target mesh. This information is then gathered and communicated to all coupler PEs, and used for redistributing the local source mesh.
Cells that intersect the bounding boxes of other processors are sent to the corresponding owner task using the aggregating {\em crystal router} algorithm that is particularly efficient in performing all-to-all strategies with $O(log(N_x))$ complexity. This graph is computed once during the setup phase to establish point-to-point communication patterns, which is then used to pack and send/receive mesh elements or data at runtime. 

This workflow guarantees that the target mesh on each processor is completely enveloped by the covering mesh repartitioned from its original source mesh decomposition, as shown in Fig.~\ref{fig:homme-coverage-mesh}. In other words, the covering mesh fully encompasses and bounds the target mesh in each task. It is important to note that some source coverage cells might be sent to multiple processors during this step, depending on the target mesh resolution and decomposition. 

%
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/e3sm-homme-coverage-mesh.eps}
    \caption{Source coverage mesh fully covers local target mesh;  local intersection proceeds between the source atmosphere (Quadrangle) and the target ocean (Polygonal) grids.}
    \label{fig:homme-coverage-mesh}
\end{figure}
%

%
%\begin{figure}[H]
%    \centering
%    \includegraphics[width=0.5\textwidth]{figures/e3sm-homme-intx-mesh.eps}
%    \caption{Intersection mesh computed with the coverage and target mesh in a single process.}
%    \label{fig:homme-intx-mesh}
%\end{figure}
%

Once the relevant covering mesh is accumulated locally on each process, the intersection computation can be carried out in parallel, \underline{completely independently}, using the advancing front algorithm (Section.~(\ref{sec:adfront-algo})). After computation of the local intersection polygons, the vertices on the shared edges between processes are communicated to avoid duplication. In order to ensure consistent local conservation constraints in the weight matrix in the parallel setting, there might be a need for additional communication of ghost intersection elements to nearest neighbors. This extra communication step is only required for computing interpolators for flux variables, and can generally be avoided when transferring scalar fields with non-conservative bilinear or higher-order interpolations.  Note that this ghost exchange on the intersection mesh only requires nearest-neighbor communications within the coupler PEs, since the communication graph has been established a-{\em priori}.

%The array-based datastructure in MOAB provides a compact storage for mesh entities, and with the concept of entity sets, groups 

The parallel advancing front algorithm presented here to globally compute the intersection supermesh can be extended to expose finer grained parallelism using hybrid-threaded (OpenMP) programming or a task-based execution model, where each task handles a unique front in the computation queue. Such task or hybrid threaded parallelism can be employed in combination with the MPI-based mesh decompositions. Using local partitions computed with Metis and through standard coloring approaches, each thread or task can then proceed to compute the intersection elements until the front collides with another, and until all the overlap elements have been computed in each process. Such a parallel hybrid algorithm has the potential to scale well even on heterogeneous architectures and provides options to improve the computational throughput of the regridding process \citep{Lohner2014}. 


\subsection{Computation of Remapping operator with TempestRemap}
\label{ssec:remap-tempest}

% ---
For illustration, consider a scalar field $\mathbf{\vec{U}}$ discretized with standard Galerkin FEM on source $\Omega_1$ and target $\Omega_2$ meshes with different resolutions. The projection of the scalar field on the target grid is in general given as follows.

\begin{equation}
\vec{U}_2(\mathbf{\Omega_2}) = \mathbf{\Pi}_1^2 \vec{U}_1(\mathbf{\Omega_1}) %= \int_{K  \in \Omega_1} \, \mathbf{\Pi}_1^2 \vec{U}_{1,K}(\mathbf{\Omega_1}),
\label{eq:global-assembly}
\end{equation}
%

where, $\mathbf{\Pi}_1^2$ is the discrete solution interpolator of $\vec{U}$ defined on $\mathbf{\Omega_1}$ to $\mathbf{\Omega_2}$. %and $\vec{U}_{1,K}$ is the scalar field evaluated in each cell $K  \in \Omega_1$. 
This interpolator $\mathbf{\Pi}_1^2$ in Eq.~(\ref{eq:global-assembly}) is often referred to as the remapping operator, which is pre-computed in the coupled climate workflows using ESMF and TempestRemap. 
For embedded meshes, the remapping operator can be calculated exactly as a restriction or prolongation from the source to target grid. However, for general unstructured meshes and in cases where the source and target meshes are topologically different, the numerical integration to assemble $\mathbf{\Pi}_1^2$ needs to be carried out on the supermesh \citep{ullrich2015}. Since a unique source and target parent element exists for every intersection element belonging to the supermesh $\mathbf{\Omega_1} \bigcup \mathbf{\Omega_2}$, $\mathbf{\Pi}_1^2$ is assembled as the sum of local mass matrix contributions on the intersection elements, by using the consistent discretization basis for the source and target field descriptions \citep{ullrich2016}. The intersection mesh typically contains arbitrary convex polygons and hence subsequent triangulation may be necessary before evaluating the integration. This global linear operator directly couples source and target DoFs based on the participating intersection element parents \citep{ullrich2009}. 

%After suitably multiplying by a test function with piecewise polynomial expansion $\mb{b}_1^j$, the projection of the discrete solution from the source component physics to the target component physics $\mb{\Pi}(\mb{\vU}_1,\mb{\vU}_2)$, defined on the component domains $\Omega_1, \Omega_2$ for $\mb{\vU}_1$ and $\mb{\vU}_2$ respectively, can be split as a sum over integral over every cell $K_1 \in \Omega_1$. This cellwise integral needs to be computed accurately in order to obtain the field interpolator with appropriate constraints imposed for local conservation. The local cellwise integral of such a projection is then given as

%
%\begin{equation}
%\vU_1(\mb{\Omega_2}) = \mb{\pi} \vU_1(\mb{\Omega_1}),
%\mb{\Pi}(\vU_1(\mb{\Omega_1}), \vU_2(\mb{\Omega_2})) = \int_{K_1  \in \Omega_1} \, \mb{\pi}(\vU_1(\mb{\Omega_1}), \vU_2(\mb{\Omega_2})) \mb{b}_1^i(\mb{\Omega}) d\mb{\Omega}.
%\end{equation}
%%
%\vspace{-3mm} 

%
%Expanding the local solution field through the Galerkin basis,  $\mb{\vU}_{1,K} = \sum_i \mb{b}_1^i(\mb{\Omega}) \mb{\hat{u}}_1^i$ and   $\mb{\vU}_2(\mb{\Omega}) = \sum_i \mb{b}_2^i(\mb{\Omega}) \mb{\hat{u}}_2^i$, and replacing the integral by a numerical quadrature with weights $w_q$ and points $\Omega_q$ (for q=$1 \ldots N_q$) yields Eq.~(\ref{eq:weight-assembly}).
%%
%\begin{equation}
%\mb{\pi}_1^2 \vU_{1,K}(\mb{\Omega_1} = \sum\limits_{q=1}^{N_q} w_q \Big( \sum_i \mb{b}_1^i(\mb{\Omega}_q) \mb{\hat{u}}_1^i ,\,
%\sum_i \mb{b}_2^i(\mb{\Omega}_q) \mb{\hat{u}}_2^i
%\Big) 
%\mb{b}_1^i(\mb{\Omega}_q) \forall K_1 \in \Omega_1
%\label{eq:weight-assembly}
%\end{equation}
%%
%\vspace{-3mm}


%
%%
%\begin{equation}
%\Pi_{\Omega_1,\Omega_2} \mb{\vU}_2(\mb{\Omega}) = \mb{\vU}_1(\mb{\Omega}).
%\label{eq:global-assembly}
%\end{equation}
%%
%\vspace{-3mm}
%

MOAB supports point-wise FEM interpolation (bilinear and higher-order spectral) with local or global subset normalization \citep{tautges_scalable_2009}, in addition to a conservative first-order remapping scheme. But higher order conservative monotone weight computations are currently unsupported natively. To fill this gap for climate applications, and to leverage existing developments in rigorous numerical algorithms to compute the conservative weights, interfaces to TempestRemap in MOAB were added to scalably compute the remap operator in parallel, without sacrificing field discretization accuracy.
%
The MOAB interface to the E3SM component models provides access to the underlying type and order of field discretization, along with the global partitioning for the DoF numbering. Hence the projection or the weight matrix can be assembled in parallel by traversing through the intersection elements, and associating the appropriate source and target DoF parent to columns and rows respectively. The MOAB implementation uses a sparse matrix representation using the \links{https://eigen.tuxfamily.org}{Eigen3} library \citep{eigen3web} to store the local weight matrix. Except for the particular case of projection onto a target grid with cGLL description, the matrix rows do not share any contributions from the same source DoFs. This implies that for FV and dGLL target field descriptions, the application of the weight matrix does not require global collective operations and sparse matrix-vector (SpMV) applications scale ideally (still memory bandwidth limited). In the cGLL case, we perform a reduction of the parallel vector along the shared DoFs to accumulate contributions exactly. However, it is non-trivial to ensure full bit-for-bit (BFB) reproducibility during such reductions and currently, the MBTR workflow does not support exact reproducibility. Requirements for rigorous bitwise reproduction for online remapping needs careful implementation to enforce that the advancing-front intersection and weight matrix is computed in the exact same global element order, in addition to ensuring that the parallel SpMV products are reduced identically, independent of the parallel mesh decompositions. 

It is also possible to use the transpose of the remapping operator computed between a particular source and target component combination, to project the solution back to the original source grid. Such an operation has the advantage of preserving the consistency and conservation metrics originally imposed in finding the remapping operator and reduces computation cost by avoiding recomputation of the weight matrix for the new directional pair. For example, when computing the remap operator between atmosphere and ocean models (with holes), it is advantageous to use the atmosphere model as the source grid, since the advancing front seed computation may require multiple trials if the initial front begins within a hole in the source mesh. 
%
Given that the seed or the initial cell determination on the target mesh is chosen at random, the corresponding intersecting cell on the source mesh found through a linear search could be contained within a hole in the source mesh. In such a case, a new target cell is then chosen and the source cell search is repeated. Hence multiple trials may be required for the advancing front algorithm to start propagating, depending on the mesh topology and decomposition. Note that the linear search in the source mesh can easily be replaced with a Kd-tree datastructure to provide better computational complexity for cases where both source and target meshes have many holes. 
%
%Typically, the seed or the initial cell determination proceeds through a linear search. A cell in target mesh is chosen based on local ID space, and a cell corresponding to the geometric location is identified on the source mesh. If the location of the point is contained within a hole in the source mesh, a new seed is then initialized. Hence multiple trials may be required for the advancing front algorithm to start propagating. 
%
Additionally, such transpose vector applications can also make the global coupling symmetric, which may have favorable implications when pursuing implicit temporal integration schemes.  
