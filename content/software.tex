\subsection{Note on MBTR Remapper Implementation}
\label{sec:software}
\setcounter{footnote}{1}

The remapping algorithms presented in the previous section are exposed through a combination of implementations in MOAB and TempestRemap libraries. Since both the libraries are written in C++, direct inheritance of key datastructures such as the GridElements (mesh) and OfflineMap (projection weights) are available to minimize data movement between the libraries. Additionally, Fortran codes such as E3SM
can invoke computations of the intersection mesh and the remapping weights through specialized language-agnostic interfaces in MOAB: \texttt{iMOAB} \citep{sigma_v12_osti_1224985}. These interfaces offer the flexibility to query, manipulate and transfer the mesh between groups of processes that represent the component and coupler processing elements. 

Using the iMOAB interfaces, the E3SM coupler can coordinate the online remapping workflow during the setup phase of the simulation, and compute the projection operators for component and scalar or vector coupled field combinations. For each pair of coupled components, the following sequence of steps are then executed to consistently compute the remapping operator and transfer the solution fields in parallel.

\begin{enumerate}
    \item \texttt{iMOAB\_SendMesh} and \texttt{iMOAB\_ReceiveMesh}:  Send the component mesh (defined on $N_{c,l}$ processes), and receive the complete unstructured mesh copy in the coupler processes ($N_x$). This mesh migration undergoes an online mesh repartition either through a trivial decomposition scheme or with advanced Zoltan algorithms (geometric or graph partitioners)
    \item \texttt{iMOAB\_ComputeMeshIntersectionOnSphere}: The advancing front intersection scheme is invoked to compute the overlap mesh in the coupler processes
    \item \texttt{iMOAB\_CoverageGraph}: Update the parallel communication graph based on the (source) coverage mesh association in each process
    \item \texttt{iMOAB\_ComputeScalarProjectionWeights}: The remapping weight operator is computed and assembled with discretization-specific (FV, SE) calls to TempestRemap, and stored in Eigen3 SparseMatrix object
\end{enumerate}

Once the remapping operator is serialized in-memory for each coupled scalar and flux fields, this operator is then used at every timestep to compute the actual projection of the data.

\begin{enumerate}
    \item \texttt{iMOAB\_SendElementTag} and \texttt{iMOAB\_ReceiveElementTag}: Using the coverage graph computed previously, direct one-to-one communication of the field data is enabled between $N_{c,l}$ and $N_x$, before and after application of the weight operator
    \item \texttt{iMOAB\_ApplyScalarProjectionWeights}: In order to compute the field interpolation or projection from the source component to the target component, a matvec product of the weight matrix and the field vector defined on the source grid is performed. The source field vector is received from source processes $N_{c,s}$ and after weight application, the target field vector is sent to target processes $N_{c,l}$ 
\end{enumerate}

Additionally, to facilitate offline generation of projection weights, a MOAB based parallel tool \texttt{mbtempest} has been written in C++, similar to ESMF and TempestRemap (serial) standalone tools. \texttt{mptempst} can load the source and target meshes from files, in parallel, and compute the intersection and remapping weights through TempestRemap. The weights can then be written back to a SCRIP-compatible file format, for any of the supported field discretization combinations in source and destination components. Added capability to apply the weight matrix onto the source solution field vectors, and native visualization plugins in VisIt for MOAB, simplify the verification of conservation and monotonicity for complex remapping workflows. This workflow allows users to validate the underlying assumptions for remapping solution fields across unstructured grids, and can be executed in both a serial and parallel setting.

%The development of parallel NetCDF serialization of the weight matrix is in progress

%mbtempest dependencies: MPI, HDF5, NetCDF (C++), TempestRemap, Eigen3

%Utilize iMOAB interfaces to drive workflow from C/C++/Fortran
