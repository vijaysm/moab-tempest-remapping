\section{Background} 
\label{sec:background}

Conservative remapping of nonlinearly coupled solution fields is a critical task to 
ensure consistency and accuracy in climate and numerical weather prediction simulations 
\citep{slingo_2009}. While there are various ways to compute a projection of a solution 
defined on a source grid $\Omega_S$ to a target grid $\Omega_T$, the requirements 
related to global or local conservation in the remapped solution reduces the number of 
potential algorithms that can be employed for such problems. 

Depending on whether (global or local) conservation is important, and if higher-order, monotone interpolators are required, there are several consistent algorithmic options that can be used \citep{DeBoer2008}. All of these different remapping schemes usually have one of these characteristic traits: non-conservative (\textbf{NC}), globally-conservative (\textbf{GC} ) and locally-conservative (\textbf{LC}). Note that strong local-conservation prescriptions also guarantee global-conservation for the remapped fields. 

\begin{enumerate}
    \item \textbf{NC}/\textbf{GC}: Solution interpolation approximations
    \begin{itemize}
    \item \textbf{NC}: (Approximate or exact) nearest neighbor interpolation
    \item \textbf{NC/GC}: Radial Basis Function (RBF) \citep{flyer2007} interpolators and patch-based Least Squares reconstructions \citep{zienkiewicz1992, fleishman2005}
    \item \textbf{GC}: Consistent Finite Element (FE) interpolation (bilinear, biquadratic, etc) with area re-normalization
    \end{itemize}
    \item \textbf{LC}: Mass ($L_2$) and gradient-preserving ($H_1$) projections
    \begin{itemize}
    \item Embedded Finite Element (FE), Finite Difference (FD), and Finite Volume (FV) meshes in adaptive computations
    \item Intersection-based field integrators with consistent higher-order discretization \citep{Jones1999}
    \item Constrained projections to ensure conservation \citep{berger1987, aguerre2017} and monotonicity \citep{ranvcic1995}
    \end{itemize}
\end{enumerate}

Typically in climate applications, flux fields are interpolated using first-order (locally) 
conservative interpolation, while other scalar fields use non-conservative but higher-order 
interpolators (e.g. bilinear or biquadratic). 
For scalar solutions that do not need to be conserved, consistent FE interpolation, 
patch-wise reconstruction schemes \citep{fornberg2008} or even nearest neighbor 
interpolation \citep{blanco2014nanoflann} can be performed efficiently using Kd-tree 
based search-and-locate point infrastructure. 
Vector fields like velocities or wind stresses are interpolated 
using these same routines by separately tackling each Cartesian-decomposed 
component of the field. 
However, conservative remapping of flux fields require computation of a supermesh 
\citep{farrell_2011}, or a global intersection mesh that can be viewed as $\Omega_S \bigcup 
\Omega_T$, which is then used to compute projection weights that contain additional 
conservation and monotonicity constraints embedded in them. 

In general, remapping implementations have three distinct steps 
to accomplish the projection of solution fields from a source to a target grid. First, the target points of interest 
are identified and located in the source grid, such that, the target cells are a subset 
of the covering (source) mesh. Next, an intersection between this covering (source) mesh 
and the target mesh is performed, in order to calculate the individual weight contribution to 
each target cell, without approximations to the component field discretizations that can be defined with arbitrary-order FV or FE basis. 
Finally, application of the weight matrix yields the projection required to 
conservatively transfer the data onto the target grid. 

To illustrate some key differences between some \textbf{NC} to \textbf{GC} or \textbf{LC} schemes, we show a 1-D Gaussian hill solution, projected onto
a coarse grid through linear basis interpolation and weighted Least-Squares ($L_2$) minimization, as shown in Fig.~\ref{fig:interpolation-gaussian-hill}. While the point-wise linear interpolator is
computationally efficient, and second-order accurate (Fig.~\ref{fig:interpolation-gaussian-hill}-(a)) for smooth profiles, it does not preserve the exact area under the curve. In contrast, the
$L_2$ minimizer conserves the global integral area, but can exhibit spurious oscillatory modes as shown in Fig.~\ref{fig:interpolation-gaussian-hill}-(b), when
dealing with solutions with strong gradients (Gibbs phenomena \citep{gottlieb1997}). This demonstration confirms that even for the simple 1-D example, a conservative and monotonic projector is necessary to preserve both
stability and accuracy for repeated remapping operator applications, in order to accurately transfer fields between grids with very different resolutions. These requirements are magnified manyfold when dealing with real-world climate simulation data.

%
\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{figures/Interpolated_Minimized_Gauss_10_010.eps}
    \caption{An illustration: comparing point interpolation vs $L_2$ minimization; impact on conservation and monotonicity properties.}
    \label{fig:interpolation-gaussian-hill}
\end{figure}
%

While there is a delicate balance in optimizing the computational efficiency of these operations without sacrificing the 
numerical accuracy or consistency of the procedure, several researchers have implemented 
algorithms that are useful for a variety of problem domains. 
In the recent years, the growing interest to rigorously tackle coupled multiphysics applications has led to research efforts focused on developing new regridding algorithms. The Data Transfer Kit (\links{https://github.com/ORNL-CEES/DataTransferKit}{DTK}) \citep{dtk_slattery_2013} from 
Oak Ridge National Labs was originally developed for Nuclear engineering applications, but has 
been extended for other problem domains through custom adaptors for meshes. DTK is more 
suited for non-conservative interpolation of scalar variables with either mesh-aware (using 
consistent discretization bases) or RBF-based meshless (point-cloud) representations 
\citep{dtk_slattery_2016} that can be extended to model transport schemes on a sphere \citep{flyer2007}.
The \links{https://github.com/laristra/portage}{Portage} library \citep{herring_2017} from Los Alamos National 
Laboratory also provides several key capabilities that are useful for geology and geophysics modeling applications 
including porous flow and seismology systems. Using advanced clipping algorithms to compute the intersection of 
axis-aligned squares/cubes against faces of a triangle/tetrahedron in 2-d and 3-d respectively, general intersections 
of arbitrary convex polyhedral domains can be computed efficiently \citep{r3d_powell_2015}. 
Support for conservative solution transfer between grids and bound-preservation (to ensure monotonicity) \citep{certik_2017} has also been recently added.
While Portage does support hybrid level parallelism (MPI + OpenMP), demonstrations on large-scale machines to compute remapping weights for climate science applications has not been pursued previously. Based on the software package documentation, support for remapping of vector fields with conservation constraints in DTK and Portage is not directly available for use in climate workflows. Additionally, unavailability of native support for projection of high-order spectral-element data on a sphere onto a target mesh restricts the use of these tools for certain component models in E3SM.

In earth science applications, the state-of-science regridding tool that is often used by many 
researchers is the \links{https://www.earthsystemcog.org/projects/esmf}{\sc ESMF} library, and the set of utility tools that are distributed along with it \citep{collins2005, dunlap2013}, to simplify the traditional offline-online computational workflow as described in Section.~(\ref{subsec:remapping-workflow}). ESMF is implemented in a component architecture \citep{zhou2006} and 
provides capabilities to generate the remapping weights for different discretization 
combinations on the source and target grids in serial and parallel. ESMF provides a standalone tool, \links{https://www.earthsystemcog.org/projects/regridweightgen}{\sc ESMF\_RegridWeightGen}, to generate {\em offline} weights 
that can be consumed by climate applications such as E3SM. ESMF also exposes interfaces that enable drivers to directly invoke the remapping algorithms in order to enable the fully-online workflow as well.

Currently, the E3SM components are integrated together in a hub-and-spoke model (Fig.~\ref{fig:e3sm-workflow-oldnew} (left)), with the inter-model communication being handled by the Model Coupling Toolkit (MCT) \citep{mct_larson2001,jacob2005m} in CIME. 
The MCT library consumes the offline weights generated with ESMF or similar tools, and
provides the functionality to interface with models, decompose the field data, and apply 
the remapping weights loaded from a file during the setup phase. Hence, MCT serves 
to abstract the communication of data in the E3SM ecosystem. However, without the offline
remapping weight generation phase for fixed grid resolutions and model combinations, the 
workflow in Fig.~\ref{fig:e3sm-workflow-oldnew} (a) is incomplete. 

Similar to the CIME-MCT driver used by E3SM, OASIS3-MCT \citep{valcke2013oasis3, craig2017} is a coupler used by many European climate models, where the interpolation weights
can be generated offline through SCRIP (included as part of OASIS3-MCT). 
An option to call SCRIP in an online mode is also available.  
The OASIS team have recently parallelized SCRIP to speed up its calculation time \citep{oasis-mct}. 
OASIS3-MCT also supports application of global conservation operations after interpolation, and does not require a strict hub-and-spoke coupler.  Similar to the coupler in CIME, OASIS3-MCT utilizes MCT to perform both the communication of fields between components and for application of the pre-computed interpolation weights in parallel.

ESMF and SCRIP traditionally handle only cell-centered data that targets Finite Volume discretizations (FV to FV projections), with first or second order conservation constraints. Hence, generating remapping weights for 
atmosphere-ocean grids with a Spectral Element (SE) source grid definition requires 
generation of an intermediate and spectrally equivalent, `{\em dual}' grid, which matches the areas of the polygons to the weight of each Gauss-Lobatto-Legendre (GLL) nodes \citep{gll-dual-mesh}. Such procedures add more steps to the offline process and can  
degrade the accuracy in the remapped solution since the original spectral order is neglected (transformation from $p$-order to first order). These procedures may also introduce numerical uncertainty in the coupled solution that could produce high solution dispersion \citep{ullrich2016}. 
%analyze the underlying effects on stability, especially during long-time integration in the coupled system.
%\comment{Rob: Can you re-check the above sentence ?  Done: I don't know about the dispersion}

To calculate remapping weights directly for high-order Spectral Element grids, E3SM uses the
\links{https://github.com/ClimateGlobalChange/tempestremap}{TempestRemap} C++ library \citep{ullrich_2013}.
TempestRemap is a uni-process tool focused on the mathematically rigorous implementations of the remapping algorithms \citep{ullrich2015, ullrich2016} and provides higher order conservative and monotonicity preserving interpolators with different discretization basis such as (Finite Volume (FV), the spectrally equivalent continuous Galerkin FE with GLL basis (cGLL), and dis-continuous Galerkin FE with GLL basis (dGLL)). This library was developed as part of the effort to fill the gap in generating consistent remapping operators for non-FV discretizations without a need for intermediate dual meshes. Computation of conservative interpolators between any combination of these discretizations (FV, cGLL, dGLL) and grid definitions are supported by TempestRemap library. However, since this regridding tool can only be executed in serial, the usage of TempestRemap prior to the work presented here has been restricted primarily to generating the required mapping weights in the offline stage.  %Section.~(\ref{subsec:remapping-workflow}).

%3 primary questions: 
%\begin{itemize}
%    \item To conserve or not to conserve ?
%    \item Offine vs Online ?
%    \item Serial vs Parallel ?
%\end{itemize}


% A focused effort to improve the efficiency of remapping computations in atmospheric calculations, through a performance portable way was explored in \citep{lee2016}. Use of task-based parallelism constructs to exploit finer grained overlap of memory access and computation on heterogeneous architectures certainly offers several opportunities to enhance speedup of the remapping calculations. By utilizing a spherical intersection and quadrature computation accelerated with the Kokkos library, good scalability was demonstrated for atmospheric tracer transport calculations similar to CSLAM \citep{Lauritzen2010}. 
% https://cfwebprod.sandia.gov/cfdocs/CompResearch/templates/insert/newsitem.cfm?news=1062


Even though ESMF and OASIS3-MCT have been used in online remapping studies, weight generation as part of a pre-processing step currently remains the preferred workflow for many production climate models.
While this decoupling provides flexibility in terms of choice of remapping tools, the data management of the mapping files for different discretizations, field constraints and grids can render provenance, reproducibility and experimentation a difficult task.  It also precludes the ability to handle moving or dynamically adaptive meshes in coupled simulations. However, it should be noted that the shift of the remapping computation process from a pre-processing stage in the workflow, to the simulation stage, imposes additional onus on the users to better understand the underlying component grid properties, their decompositions, the solution fields being transferred and the preferred options for computing the weights. This also raises interesting workflow modifications to ensure verification of the online weights such that consistency, conservation and dissipation of key fields are within user-specified constraints. In the implementation discussed here, the online remapping computation uses the exact same input grids, and specifications like the offline workflow, along with ability to write the weights to file, which can be used to run detailed verification studies as needed. 

There are several challenges in scalably computing the regridding operators in parallel, since
it is imperative to have both a mesh- and partition-aware datastructure to handle this part of the regridding workflow.
%In this context, the GFDL and Bespoke Framework Generator have also been applied in some production cases for coupled climate simulations \citep{gmd-5-1589-2012}. However,
A few climate models have begun to calculate weights online as part of their regular operation.
The ICON GCM \citep{Wan2013} uses YAC \citep{yac_2016} and FGOALS \citep{Li2013} uses
the C-Coupler \citep{ccoupler1_2014,ccoupler2_2018} framework. These
codes expose both offline and online remapping capabilities with parallel decomposition management similar to the ongoing effort presented in the current work for E3SM. Both of these packages provide algorithmic options to perform in-memory search and locate operations, interpolation of field data between meshes with first order conservative remapping, higher-order patch-recovery \citep{zienkiewicz1992} and RBF schemes and the NC nearest-neighbor queries. The use of non-blocking communication for field data in these packages align closely with scalable strategies implemented in MCT \citep{mct_jacob_2005}.
While these capabilities are used routinely in production runs for their respective models, the motivation
for the work presented here is to tackle coupled high-resolution runs on next generation architectures with scalable algorithms (the high resolution E3SM coupler routinely runs on 13,000 mpi tasks), without sacrificing
numerical accuracy for all discretization descriptions (FV, cGLL, dGLL) on unstructured grids.
%
%C-Coupler provides options to couple within a single executable similar to E3SM
%
%Compare MOAB interection with algo in YAC \citep{yac_2016} section 2.7
%
%Chinese Coupler: multidimensional remapping software CoR (Common Remapping) \citep{liu2013}
%The parallel decomposition manager is analogous to what MCT does
%CoR supports both offline and online modes for remapping weight
%
%MOAB performs much better than CoR and decomposition manager in CCoupler \citep{ccoupler1_2014}

In the E3SM workflow supported by CIME, the ESMF-regridder understands the component grid definitions, and generates the weight matrices (offline).
The CIME driver loads these operators at runtime and places them in MCT datatypes, which treat them as discrete operators to compute 
the interpolation or projection of data on the target grids. Additional changes in conservation 
requirements or monotonicity of the field data cannot be imposed as a runtime or post-processing step in
such a workflow. In the current work, we present a new infrastructure with scalable algorithms implemented using the MOAB mesh library and TempestRemap package to replace the ESMF-E3SM-MCT remapper/coupler workflow. A detailed review of the algorithmic approach used
in the MOAB-TempestRemap (MBTR) workflow, along with the software 
interfaces exposed to E3SM is presented next. 
%Verification of the field projection  accuracy convergence for some smooth cases and parallel scalability comparisons between ESMF-MCT and MBTR workflows will be presented in Section.~(\ref{sec:results}).

%Demonstration
% and discuss the scalability of two different 
%remapping implementations on large-scale machines. In the results section, we will also 
%discuss the pros and cons of using different algorithmic strategies and the need for online 
%remapping capabilities with flexible workflow mechanisms in order to optimize both 
%computational speed and numerical accuracy.
%
