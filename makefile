gmd = GMDMain
siam = SIAMMain
frontier = FrontiersMain

default: GMD
all: $(gmd).pdf $(siam).pdf $(frontier).pdf

FIGURES = $(wildcard ../figures/*.eps)
CONTENTTEX = abstract.tex introduction.tex software.tex algorithmic-approach.tex conclusion.tex results.tex acknowledgement.tex
CONTENT = $(addprefix content/,$(CONTENTTEX))
FILES = paper-commands.tex Remapping.bib $(CONTENT)	$(FIGURES)
AUX_gmd = copernicus.cls copernicus.bst copernicus.cfg
AUX_siam = siamltex1213.cls siam.bst siam10.clo subeqn.clo
AUX_frontier = frontiersSCNS.cls frontiersinSCNS_ENG_HUMS.bst logo1.eps

GMD: $(gmd).pdf
$(gmd).pdf: $(gmd).tex $(FILES) $(AUX_gmd)
	latex $(gmd).tex
	bibtex $(gmd).aux
	latex $(gmd).tex
	bibtex $(gmd).aux
	latex $(gmd).tex
	dvips $(gmd).dvi
	ps2pdf $(gmd).ps
#	pdflatex $(gmd).tex

SIAM: $(siam).pdf
$(siam).pdf: $(siam).tex $(FILES) $(AUX_siam)
	latex $(siam).tex
	bibtex $(siam).aux
	latex $(siam).tex
	bibtex $(siam).aux
	latex $(siam).tex
	dvips $(siam).dvi
	ps2pdf $(siam).ps
#	pdflatex $(siam).tex

FRONTIER: $(frontier).pdf
$(frontier).pdf: $(frontier).tex $(FILES) $(AUX_frontier)
	latex $(frontier).tex
	bibtex $(frontier).aux
	latex $(frontier).tex
	bibtex $(frontier).aux
	latex $(frontier).tex
	dvips $(frontier).dvi
	ps2pdf $(frontier).ps
#	pdflatex $(frontier).tex


wordcount:
	texcount content/introduction.tex content/background.tex content/algorithmic-approach.tex content/results.tex content/conclusion.tex content/acknowledgement.tex 

clean:
	rm -f $(gmd).dvi $(gmd).log $(gmd).out $(gmd).bbl $(gmd).aux $(gmd)-blx.bib $(gmd).blg $(gmd).ps $(gmd).pdf

cleanall:
	rm -f $(gmd).dvi $(gmd).log $(gmd).out $(gmd).bbl $(gmd).aux $(gmd)-blx.bib $(gmd).blg $(gmd).ps $(gmd).pdf
	rm -f $(siam).dvi $(siam).log $(siam).out $(siam).bbl $(siam).aux $(siam)-blx.bib $(siam).blg $(siam).ps $(siam).pdf
	rm -f $(frontier).dvi $(frontier).log $(frontier).out $(frontier).bbl $(frontier).aux $(frontier)-blx.bib $(frontier).blg $(frontier).ps $(frontier).pdf

dist:
	tar czf $(gmd).tgz $(gmd).tex  $(FILES) makefile

